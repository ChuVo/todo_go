# ToDo_Go
The application is written as a training project in the study of GO

### Start application development mode
```
go run ./cmd/web -mode="dev"
```

## MySQL

### Создание БД

```
CREATE DATABASE crazybox CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
```

### Посмотреть список всех БД
```
SHOW DATABASES;
```

### Посмотреть список всех таблиц в БД
```
SHOW TABLES;
```

### Удалить таблицу из БД
```
DROP TABLE Name;
```

### Создать таблицу crazybox
```
CREATE TABLE crazybox (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    title TEXT NOT NULL,
    content TEXT NOT NULL,
    created DATETIME NOT NULL,
    expires DATETIME NOT NULL
);
```

### Добавление индекса для созданного столбца
```
CREATE INDEX idx_snippets_created ON snippets(created);
```

### Добавление одного столбца в таблицу
```
ALTER TABLE [Table] ADD COLUMN [colmn_name] [column_type] NOT NULL;
```

### Добавление записи в таблицу
```
INSERT INTO snippets (title, content, created, expires) VALUES ('Не откладывай на завтра', 'Не откладывай на завтра, что можешь сделать сегодня.', UTC_TIMESTAMP(), DATE_ADD(UTC_TIMESTAMP(), INTERVAL 7 DAY));
```

### Получить записи в таблицу
```
SELECT id, title, expires FROM snippets;
```

### Как посмотреть список таблиц и их структуру в MySQL
```
SHOW DATABASES; - список баз данных
SHOW TABLES [FROM db_name]; -  список таблиц в базе 
SHOW COLUMNS FROM таблица [FROM db_name]; - список столбцов в таблице
SHOW CREATE TABLE table_name; - показать структуру таблицы в формате "CREATE TABLE"
SHOW INDEX FROM tbl_name; - список индексов
SHOW GRANTS FOR user [FROM db_name]; - привилегии для пользователя.
SHOW VARIABLES; - значения системных переменных
SHOW [FULL] PROCESSLIST; - статистика по mysqld процессам
SHOW STATUS; - общая статистика
SHOW TABLE STATUS [FROM db_name]; - статистика по всем таблицам в базе
```

## Дополнительно

Если локально нет пользователя, то добавить
```
CREATE USER 'web'@'localhost';
```

Дать права доступа
```azure
GRANT SELECT, INSERT, UPDATE, DELETE ON crazybox.* TO 'web'@'localhost';
```

Изменить пароль
```
-- Важно: Не забудьте заменить 'pass' на свой пароль, иначе это и будет паролем.
ALTER USER 'web'@'localhost' IDENTIFIED BY 'pass';
```

