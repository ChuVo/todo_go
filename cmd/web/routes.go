package main

import "net/http"

func (app *application) routes() *http.ServeMux {
	mux := http.NewServeMux()
	mux.HandleFunc("/", app.home)

	mux.HandleFunc("/snippets", app.showSnippets)
	mux.HandleFunc("/snippets/create", app.createSnippet)
	mux.HandleFunc("/snippets/data", app.showSnippet)
	mux.HandleFunc("/snippets/delete", app.deleteSnippet)
	mux.HandleFunc("/snippets/edite", app.editeSnippet)

	fileServer := http.FileServer(http.Dir("./ui/static/"))
	mux.Handle("/static/", http.StripPrefix("/static", fileServer))

	return mux
}
