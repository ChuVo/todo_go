package main

import (
	"errors"
	"fmt"
	"gitlab.com/ChuVo/todo_go/pkg/models"
	"log"
	"net/http"
	"strconv"
)

func (app *application) home(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		app.notFound(w)
		return
	}

	_, err := app.snippets.Latest()
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.render(w, r, "home.page.tmpl", nil)
}

func (app *application) showSnippets(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/snippets" {
		app.notFound(w)
		return
	}

	s, err := app.snippets.Latest()
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.render(w, r, "snippets.page.tmpl", &templateData{
		Snippets: s,
	})
}

func (app *application) showSnippet(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(r.URL.Query().Get("id"))

	if err != nil || id < 1 {
		app.notFound(w)
		return
	}

	s, err := app.snippets.Get(id)
	if err != nil {
		if errors.Is(err, models.ErrNoRecord) {
			app.notFound(w)
		} else {
			app.serverError(w, err)
		}
		return
	}

	app.render(w, r, "snippet.page.tmpl", &templateData{
		Snippet: s,
	})
}

func (app *application) createSnippet(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		app.render(w, r, "snippetCreate.page.tmpl", &templateData{})
		return
	}

	err := r.ParseForm()
	if err != nil {
		log.Println(err)
	}
	title := r.FormValue("title")
	content := r.FormValue("content")
	expires := r.FormValue("expires")

	fmt.Println("log", title, content)

	id, err := app.snippets.Insert(title, content, expires)
	if err != nil {
		app.serverError(w, err)
		return
	}

	http.Redirect(w, r, fmt.Sprintf("/snippets/data?id=%d", id), http.StatusSeeOther)
}

func (app *application) deleteSnippet(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/snippets/delete" {
		return
	}

	id, err := strconv.Atoi(r.URL.Query().Get("id"))
	if err != nil || id < 1 {
		fmt.Println("id", id)
		app.notFound(w)
		return
	}

	_, err = app.snippets.Delete(id)
	if err != nil {
		if errors.Is(err, models.ErrNoRecord) {
			app.notFound(w)
		} else {
			app.serverError(w, err)
		}
		return
	}

	http.Redirect(w, r, fmt.Sprintf("/"), http.StatusSeeOther)
}

func (app *application) editeSnippet(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Edite page")
	id, err := strconv.Atoi(r.URL.Query().Get("id"))
	if r.Method != http.MethodPut {
		s, err := app.snippets.Get(id)
		if err != nil {
			if errors.Is(err, models.ErrNoRecord) {
				app.notFound(w)
			} else {
				app.serverError(w, err)
			}
			return
		}

		fmt.Println("Snippet", s)
		app.render(w, r, "edite.page.tmpl", &templateData{
			Snippet: s,
		})
		return
	}

	//id, err := strconv.Atoi(r.URL.Query().Get("id"))
	if err != nil || id < 1 {
		fmt.Println("id", id)
		app.notFound(w)
		return
	}

	err = r.ParseForm()
	if err != nil {
		log.Println(err)
	}

	title := r.FormValue("title")
	content := r.FormValue("content")
	expires := r.FormValue("expires")

	fmt.Println(id, title, expires)

	_, err = app.snippets.Update(id, title, content, expires)
	if err != nil {
		app.serverError(w, err)
		return
	}

	http.Redirect(w, r, fmt.Sprintf("/snippet?id=%d", id), http.StatusSeeOther)
}
